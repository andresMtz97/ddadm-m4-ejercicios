package com.aamg.modulo4ejercicios.tarea

import java.io.Serializable

class User(
    val name: String,
    val lastname: String,
    val email: String,
    val password: String,
    val gender: String
): Serializable