package com.aamg.modulo4ejercicios.tarea

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityTareaBinding

class TareaActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTareaBinding
    private var name: String = ""
    private var lastname: String = ""
    private var email: String = ""
    private var password: String = ""
    private var gender: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTareaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.etName.addTextChangedListener {
            name = it.toString()
            binding.etName.error = null
        }
        binding.etLastname.addTextChangedListener {
            lastname = it.toString()
            binding.etLastname.error = null
        }
        binding.etEmail.addTextChangedListener {
            email = it.toString()
            binding.etEmail.error = null
        }
        binding.etPassword.addTextChangedListener {
            password = it.toString()
            binding.etPassword.error = null
        }
        binding.rgGender.setOnCheckedChangeListener { _, checkedId ->
            binding.rbMale.error = null
            binding.rbFemale.error = null
            gender = when(checkedId) {
                R.id.rbMale -> "Masculino"
                R.id.rbFemale -> "Femenino"
                else -> ""
            }
        }

        binding.btnSubmit.setOnClickListener { navigateToInfo() }
    }

    private fun navigateToInfo() {
        if (validateForm()) {
            val intent = Intent(this, ShowInfoActivity::class.java).apply {
                putExtra("EXTRA_USER", User(name, lastname, email, password, gender))
            }
            startActivity(intent)
        } else {
            Toast.makeText(this, "Form inválido", Toast.LENGTH_SHORT).show()
        }
    }

    private fun validateForm(): Boolean {
        var isValid = true
        // gender = when(binding.rgGender.checkedRadioButtonId)

        if (name.isBlank()) {
            isValid = false
            binding.etName.error = getString(R.string.required)
        }
        if (lastname.isBlank()) {
            isValid = false
            binding.etLastname.error = getString(R.string.required)
        }
        if (email.isBlank()) {
            isValid = false
            binding.etEmail.error = getString(R.string.required)
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            isValid = false
            binding.etEmail.error = "Correo electrónico inválido."
        }
        if (password.isBlank()) {
            isValid = false
            binding.etPassword.error = getString(R.string.required)
        }
        if (gender.isBlank()) {
            isValid = false
            binding.rbMale.error = getString(R.string.required)
            binding.rbFemale.error = getString(R.string.required)
        }

        return isValid
    }
}