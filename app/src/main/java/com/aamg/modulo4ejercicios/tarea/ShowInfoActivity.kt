package com.aamg.modulo4ejercicios.tarea

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityShowInfoBinding

class ShowInfoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShowInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShowInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val usuario = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getSerializableExtra("EXTRA_USER", User::class.java)
        } else {
            intent.getSerializableExtra("EXTRA_USER") as User
        }

        binding.tvName.text = usuario?.name
        binding.tvLastname.text = usuario?.lastname
        binding.tvEmail.text = usuario?.email
        binding.tvPassword.text = "?"
        binding.tvGenre.text = usuario?.gender
    }
}