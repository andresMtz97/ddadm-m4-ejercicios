package com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityRelativeLayoutBinding

class RelativeLayoutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRelativeLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRelativeLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnBack.setOnClickListener{ finish() }
    }
}