package com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityRecyclerViewBinding

class RecyclerViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRecyclerViewBinding
    private val cuentas: ArrayList<Cuenta> = arrayListOf(
        Cuenta(1, "BBVA", 10132.56),
        Cuenta(2, "Santander Like U", 856.23),
        Cuenta(3, "HSBC", 7563.22),
        Cuenta(4, "Scotiabank", 100.00),
        Cuenta(5, "Broxel", 500.00),
        Cuenta(6, "NU", 12345.56),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecyclerViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val adapter = CuentaAdapter(cuentas)

        adapter.onClickDelete = {
            Toast.makeText(
                this,
                "Eliminando cuenta con id: ${it.cuentaId}",
                Toast.LENGTH_SHORT
            ).show()
        }
        binding.rvCuentas.adapter = adapter
        binding.rvCuentas.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )

    }
}