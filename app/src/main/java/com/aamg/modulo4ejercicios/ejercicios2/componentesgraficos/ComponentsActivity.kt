package com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityComponentsBinding

class ComponentsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityComponentsBinding
    private val cuentas = arrayListOf("-- Seleccione una opción --", "BBVA", "Santander", "HSBC", "NU")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityComponentsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cuentas)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spCuenta.adapter = adapter

        binding.btnClose.setOnClickListener { finish() }

    }
}