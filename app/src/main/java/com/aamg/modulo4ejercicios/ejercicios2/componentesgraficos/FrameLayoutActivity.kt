package com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityFrameLayoutBinding

class FrameLayoutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFrameLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFrameLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnClickMe.setOnClickListener {
            Toast.makeText(this, "May the force be with you", Toast.LENGTH_LONG).show()
        }
    }
}