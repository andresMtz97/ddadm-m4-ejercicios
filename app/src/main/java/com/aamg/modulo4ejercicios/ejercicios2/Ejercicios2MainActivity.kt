package com.aamg.modulo4ejercicios.ejercicios2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aamg.modulo4ejercicios.databinding.ActivityEjercicios2MainBinding
import com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.ComponentsActivity
import com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.ConstraintLayoutActivity
import com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.FrameLayoutActivity
import com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.LinearLayoutActivity
import com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.recyclerview.RecyclerViewActivity
import com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.RelativeLayoutActivity

class Ejercicios2MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEjercicios2MainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEjercicios2MainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnFrameLayout.setOnClickListener { navigateToFrameLayout() }
        binding.btnLinearLayout.setOnClickListener { navigateToLinearLayout() }
        binding.btnRelativeLayout.setOnClickListener { navigateToRelativeLayout() }
        binding.btnConstraintLayout.setOnClickListener { navigateToConstrintLayout() }
        binding.btnComponents.setOnClickListener { navigateToComponents() }
        binding.btnRecyclerView.setOnClickListener { naavigateToRecyclerView() }
    }

    private fun naavigateToRecyclerView() {
        val intent = Intent(this, RecyclerViewActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToComponents() {
        val intent = Intent(this, ComponentsActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToConstrintLayout() {
        val intent = Intent(this, ConstraintLayoutActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToRelativeLayout() {
        val intent = Intent(this, RelativeLayoutActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToLinearLayout() {
        val intent = Intent(this, LinearLayoutActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToFrameLayout() {
        val intent = Intent(this, FrameLayoutActivity::class.java)
        startActivity(intent)
    }
}