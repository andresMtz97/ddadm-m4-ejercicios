package com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.recyclerview

data class Cuenta(
    val cuentaId: Int,
    val nombre: String,
    val saldo: Double
)
