package com.aamg.modulo4ejercicios.ejercicios2.componentesgraficos.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.aamg.modulo4ejercicios.R
import java.text.DecimalFormat

class CuentaAdapter(
    private val list: ArrayList<Cuenta>
) : Adapter<CuentaViewHolder>() {

    var onClickDelete: ((Cuenta) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CuentaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.recyclerview_item_cuenta,
            parent,
            false
        )
        return CuentaViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: CuentaViewHolder, position: Int) {
        holder.render(list[position], onClickDelete)
    }

}

class CuentaViewHolder(view: View): ViewHolder(view) {
    val tvNombre = view.findViewById<TextView>(R.id.tvNombre)
    val tvSaldo = view.findViewById<TextView>(R.id.tvSaldo)
    val ibDelete = view.findViewById<ImageButton>(R.id.ibDelete)
    val df = DecimalFormat("¤ ###,###.##")

    fun render(cuenta: Cuenta, onClickDelete: ((Cuenta) -> Unit)?) {
        tvNombre.text = cuenta.nombre
        tvSaldo.text = df.format(cuenta.saldo)
        ibDelete.setOnClickListener { onClickDelete?.invoke(cuenta) }
    }

}