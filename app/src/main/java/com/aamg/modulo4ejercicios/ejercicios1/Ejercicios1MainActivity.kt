package com.aamg.modulo4ejercicios.ejercicios1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityEjercicios1MainBinding
import com.aamg.modulo4ejercicios.ejercicios1.ciclovida.CicloVidaActivity
import com.aamg.modulo4ejercicios.ejercicios1.extras.SendExtrasActivity
import com.aamg.modulo4ejercicios.ejercicios1.intentimplicito.IntentImplicitoActivity

class Ejercicios1MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEjercicios1MainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEjercicios1MainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCicloVida.setOnClickListener { navigateToCicloVida() }
        binding.btnIntentImplicito.setOnClickListener { navigateToIntentImplicito() }
        binding.btnPasoParametros.setOnClickListener { navigateToExtras() }
    }

    private fun navigateToCicloVida() {
        val intent = Intent(this, CicloVidaActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToIntentImplicito() {
        val intent = Intent(this, IntentImplicitoActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToExtras() {
        val intent = Intent(this, SendExtrasActivity::class.java)
        startActivity(intent)
    }
}