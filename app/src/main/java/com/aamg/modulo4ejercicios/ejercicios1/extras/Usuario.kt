package com.aamg.modulo4ejercicios.ejercicios1.extras

import java.io.Serializable

class Usuario (
    val name: String,
    val lastname: String,
    val username: String
): Serializable