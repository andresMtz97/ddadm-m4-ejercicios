package com.aamg.modulo4ejercicios.ejercicios1.extras

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivitySendExtrasBinding

class SendExtrasActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySendExtrasBinding
    private val resultRegister = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            Toast.makeText(this, result.data?.getStringExtra("EXTRA_INFO"), Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "Sin información extra", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySendExtrasBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSendExtras.setOnClickListener { sendExtras() }
    }

    private fun sendExtras() {
        val cuentas: Array<Cuenta> = arrayOf(
            Cuenta(1, "BBVA", 10132.56),
            Cuenta(2, "Santander Like U", 856.23),
            Cuenta(3, "HSBC", 7563.22)
        )
        val intent = Intent(this, ReceiveExtrasActivity::class.java).apply {
            putExtra("EXTRA_USUARIO", Usuario("Andrés", "Martínez", "andresMtz"))
            putExtra("EXTRA_CUENTAS", cuentas)
        }
        resultRegister.launch(intent)
    }
}