package com.aamg.modulo4ejercicios.ejercicios1.extras

import java.io.Serializable

class Cuenta (
    val cuentaId: Int,
    val nombre: String,
    val saldo: Double
): Serializable