package com.aamg.modulo4ejercicios.ejercicios1.extras

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityReceiveExtrasBinding
import com.aamg.modulo4ejercicios.databinding.ActivitySendExtrasBinding

class ReceiveExtrasActivity : AppCompatActivity() {

    private lateinit var binding: ActivityReceiveExtrasBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReceiveExtrasBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val usuario = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getSerializableExtra("EXTRA_USUARIO", Usuario::class.java)
        } else {
            intent.getSerializableExtra("EXTRA_USUARIO") as Usuario
        }

        val cuentas = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getSerializableExtra("EXTRA_CUENTAS", Array<Cuenta>::class.java)
        } else {
            intent.getSerializableExtra("EXTRA_CUENTAS") as Array<Cuenta>
        }

        binding.tvUsuario.text = "Hola ${usuario?.name ?: "Usuario" } ${usuario?.lastname ?: "" } (${usuario?.username ?: "invitado"})"

        binding.tvCuenta0Nombre.text = cuentas?.get(0)?.nombre ?: "Undefined"
        binding.tvCuenta0Saldo.text = "Saldo: ${ cuentas?.get(0)?.saldo ?: "Undefined" }"

        binding.tvCuenta1Nombre.text = cuentas?.get(1)?.nombre ?: "Undefined"
        binding.tvCuenta1Saldo.text = "Saldo: ${ cuentas?.get(1)?.saldo ?: "Undefined" }"

        binding.tvCuenta2Nombre.text = cuentas?.get(2)?.nombre ?: "Undefined"
        binding.tvCuenta2Saldo.text = "Saldo: ${ cuentas?.get(2)?.saldo ?: "Undefined" }"

        binding.btnReturnInfo.setOnClickListener { returnExtras() }
    }

    private fun returnExtras() {
        val intent = Intent().apply {
            putExtra("EXTRA_INFO", "Esta es información extra: Consultó correctamente sus cuentas")
        }
        setResult(RESULT_OK, intent)
        finish()
    }
}