package com.aamg.modulo4ejercicios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aamg.modulo4ejercicios.databinding.ActivityMainBinding
import com.aamg.modulo4ejercicios.ejercicios1.Ejercicios1MainActivity
import com.aamg.modulo4ejercicios.ejercicios2.Ejercicios2MainActivity
import com.aamg.modulo4ejercicios.practicafinal.LoginActivity
import com.aamg.modulo4ejercicios.tarea.TareaActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEjercicios1.setOnClickListener { navigateToEjercicios1() }
        binding.btnEjercicios2.setOnClickListener { navigateToEjercicios2() }
        binding.btnTarea1.setOnClickListener { navigateToTarea() }
        binding.btnPracticaFinal.setOnClickListener { navigateToPracticaFinal() }
    }

    private fun navigateToPracticaFinal() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToTarea() {
        val intent = Intent(this, TareaActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToEjercicios1() {
        val intent = Intent(this, Ejercicios1MainActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToEjercicios2() {
        val intent = Intent(this, Ejercicios2MainActivity::class.java)
        startActivity(intent)
    }
}