package com.aamg.modulo4ejercicios.practicafinal.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.FragmentSignupBinding
import com.aamg.modulo4ejercicios.practicafinal.HomeActivity


class SignupFragment : Fragment() {

    private lateinit var binding: FragmentSignupBinding
    private var name: String = ""
    private var lastname: String = ""
    private var email: String = ""
    private var password: String = ""
    private var gender: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSignupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.etName.addTextChangedListener {
            name = it.toString()
            binding.etName.error = null
        }
        binding.etLastname.addTextChangedListener {
            lastname = it.toString()
            binding.etLastname.error = null
        }
        binding.etEmail.addTextChangedListener {
            email = it.toString()
            binding.etEmail.error = null
        }
        binding.etPassword.addTextChangedListener {
            password = it.toString()
            binding.etPassword.error = null
        }
        binding.rgGender.setOnCheckedChangeListener { _, checkedId ->
            binding.rbMale.error = null
            binding.rbFemale.error = null
            gender = when(checkedId) {
                R.id.rbMale -> "Masculino"
                R.id.rbFemale -> "Femenino"
                else -> ""
            }
        }

        binding.btnSubmit.setOnClickListener { navigateToHome() }
    }

    private fun navigateToHome() {
        if (validateForm()) {
            val bundle = Bundle().apply {
                putString("name", name)
                putString("lastname", lastname)
                putString("email", email)
                putString("password", password)
                putString("gender", gender)
            }
            val intent = Intent(requireContext(), HomeActivity::class.java).apply {
                putExtra("EXTRA_SIGNUP", bundle)
            }
            startActivity(intent)
        } else {
            Toast.makeText(requireContext(), getString(R.string.invalid_form), Toast.LENGTH_SHORT).show()
        }
    }

    private fun validateForm(): Boolean {
        var isValid = true
        // gender = when(binding.rgGender.checkedRadioButtonId)

        if (name.isBlank()) {
            isValid = false
            binding.etName.error = getString(R.string.required)
        }
        if (lastname.isBlank()) {
            isValid = false
            binding.etLastname.error = getString(R.string.required)
        }
        if (email.isBlank()) {
            isValid = false
            binding.etEmail.error = getString(R.string.required)
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            isValid = false
            binding.etEmail.error = getString(R.string.invalid_email)
        }
        if (password.isBlank()) {
            isValid = false
            binding.etPassword.error = getString(R.string.required)
        }
        if (gender.isBlank()) {
            isValid = false
            binding.rbMale.error = getString(R.string.required)
            binding.rbFemale.error = getString(R.string.required)
        }
        return isValid
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            SignupFragment()
    }
}