package com.aamg.modulo4ejercicios.practicafinal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Home (Práctica final)"

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (intent.hasExtra("EXTRA_LOGIN")) {
            intent.getBundleExtra("EXTRA_LOGIN").apply {
                binding.tvInfo.text = "Información de login\nEmail: ${this?.getString("email")}\nPassword: ${this?.getString("password")}"
            }
        } else if (intent.hasExtra("EXTRA_SIGNUP")) {
            intent.getBundleExtra("EXTRA_SIGNUP").apply {
                binding.tvInfo.text = "Información de registro\n" +
                        "Nombre: ${this?.getString("name")}\n" +
                        "Apellido: ${this?.getString("lastname")}\n" +
                        "Email: ${this?.getString("email")}\n" +
                        "Password: ${this?.getString("password")}\n" +
                        "Sexo: ${this?.getString("gender")}\n"
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressedDispatcher.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}