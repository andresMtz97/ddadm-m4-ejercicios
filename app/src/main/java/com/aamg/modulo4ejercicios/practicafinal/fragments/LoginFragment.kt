package com.aamg.modulo4ejercicios.practicafinal.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.aamg.modulo4ejercicios.R
import com.aamg.modulo4ejercicios.databinding.FragmentLoginBinding
import com.aamg.modulo4ejercicios.practicafinal.HomeActivity

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private var email: String = "";
    private var password: String = "";

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvSignup.setOnClickListener { navigateToSignup() }
        binding.etEmail.addTextChangedListener {
            email = it.toString()
            binding.etEmail.error = null
        }
        binding.etPassword.addTextChangedListener {
            password = it.toString()
            binding.etPassword.error = null
        }
        binding.btnSend.setOnClickListener { navigateToHome() }
    }

    private fun navigateToHome() {
        if (validateForm()) {
            val bundle = Bundle().apply {
                putString("email", email)
                putString("password", password)
            }
            val intent = Intent(requireContext(), HomeActivity::class.java).apply {
                putExtra("EXTRA_LOGIN", bundle)
            }
            startActivity(intent)
        } else {
            Toast.makeText(requireContext(), getString(R.string.invalid_form), Toast.LENGTH_SHORT).show()
        }
    }

    private fun validateForm(): Boolean {
        var isValid = true

        if (email.isBlank()) {
            isValid = false
            binding.etEmail.error = getString(R.string.required)
        }
        if (password.isBlank()) {
            isValid = false
            binding.etPassword.error = getString(R.string.required)
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            isValid = false
            binding.etEmail.error = getString(R.string.invalid_email)
        }

        return isValid
    }

    private fun navigateToSignup() {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, SignupFragment.newInstance())
            .addToBackStack("SignupFragment")
            .commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            LoginFragment()
    }
}